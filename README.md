## Requirements
- Python 3
- Jupyter Notebook

Install the required Python modules by running the following command:

``` 
$ pip3 install -r requirements.txt
```
